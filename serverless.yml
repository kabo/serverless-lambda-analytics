service: serverless-lambda-analytics
custom:
  webpackIncludeModules: true 
  stage: ${opt:stage, self:provider.stage}
  region: ${opt:region, self:provider.region}
  reportmailerArn: arn:aws:states:${self:custom.region}:${self:custom.config.awsAccountNo}:stateMachine:${self:service}-${self:custom.stage}-reportMailer
  config: ${file(./config.${self:custom.stage}.yml)}
  documentation:
    summary: "API to feed the analytics DB"
    description: "The API endpoints you need to register hits to your website"
    resources:
      - path: hit
        description: Registers a hit to the current page
      - path: hit.js
        description: Returns the javascript that registers a hit to the current page
      - path: hit.png
        description: Registers a hit to the current page, returns a transparent image
    models:
      - name: RegisterHit
        description: Model for registering a hit
        contentType: application/json
        schema:
          "$schema": "http://json-schema.org/draft-04/schema#"
          title: Hit
          description: A hit
          type: object
          properties:
            url: 
              description: The URL to register a hit to
              type: string
          required:
            - url
      - name: JavascriptResponse
        description: Model for javascript responses
        contentType: application/javascript
        schema:
      - name: PngResponse
        description: Model for png responses
        contentType: image/png
        schema:
provider:
  name: aws
  runtime: nodejs6.10
  memorySize: 128
  cfLogs: true
  stage: dev
  region: us-east-1
  environment:
    awsRegion: ${self:custom.region}
    allowedOriginsRegex: ${self:custom.config.allowedOriginsRegex}
    simpleDbDomain:
      Ref: AnalyticsHitsDatabase
      #${self:custom.config.simpleDbDomain}
    simpleDbEndpoint: ${self:custom.config.simpleDbEndpoint}
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - "ses:SendRawEmail"
      Resource:
        - "*"
    - Effect: "Allow"
      Action:
        - "states:StartExecution"
      Resource:
        - ${self:custom.reportmailerArn}
    - Effect: "Allow"
      Action:
        - "sdb:Select"
        - "sdb:PutAttributes"
        - "sdb:DeleteAttributes"
      Resource:
        Fn::Join:
          - ""
          - - "arn:aws:sdb:"
            - ${self:custom.region}
            - ":"
            - ${self:custom.config.awsAccountNo}
            - ":domain/"
            - "Ref": "AnalyticsHitsDatabase"
functions:
  hitreg:
    handler: handler.hit
    events:
      - http:
          path: hit
          method: POST
          cors:
            origins:
              - ${self:custom.config.allowedOrigins}
            headers:
              - Content-Type
            methods:
              - POST
          documentation:
            summary: Registers a hit
            description: Registers a hit
            requestBody:
              description: An object with information about the hit
            requestHeaders:
              - name: Origin
                description: CORS header, automatically set by the browser.
              - name: User-Agent
                description: The user agent of the browser, automatically set by the browser.
              - name: Dnt
                description: Do-not-track, if this is set to 1 then the IP or the User-Agent won't be stored. Automatically set by the browser.
            requestModels:
              "application/json": RegisterHit
            methodResponses:
              - statusCode: '204'
                description: Hit successfully registered
                responseHeaders:
                  - name: Access-Control-Allow-Origin
                    description: CORS header
              - statusCode: '403'
                description: Origin header missing or faulty
                responseHeaders:
                  - name: Access-Control-Allow-Origin
                    description: CORS header

  hitjs:
    handler: handler.hitjs
    events:
      - http:
          path: hit.js
          method: GET
          documentation:
            summary: Returns the javascript to register a hit
            description: The javascript registers a hit to /hit
            methodResponses:
              - statusCode: '200'
                description: Javascript succesfully generated
                responseBody:
                  description: Javascript that registers a hit
                responseModels:
                  "application/javascript": JavascriptResponse
  hitpng:
    handler: handler.hitpng
    events:
      - http:
          path: hit.png
          method: GET
          documentation:
            summary: Registers a hit, returns an image
            description: In case javascript is not available it's possible to use this endpoint instead
            requestHeaders:
              - name: Referer
                description: The URL to register a hit for, automatically set by the browser.
              - name: User-Agent
                description: The user agent of the browser, automatically set by the browser.
              - name: Dnt
                description: Do-not-track, if this is set to 1 then neither the IP nor the User-Agent will be stored. Automatically set by the browser.
            methodResponses:
              - statusCode: '200'
                description: Hit successfully registered
                responseBody:
                  description: A transparent 1x1 pixel png
                responseModels:
                  "image/png": PngResponse
  reportTrigger:
    handler: report.trigger
    environment:
      REPORTMAILER: ${self:custom.reportmailerArn}
    events:
      - schedule: cron(0 0 * * ? *)
  reportGetHits:
    handler: report.getHits
  reportCompileReport:
    handler: report.compileReport
  reportSendReport:
    handler: report.sendReport
    environment:
      mailReportTo: ${self:custom.config.mailReportTo}
      mailReportFrom: ${self:custom.config.mailReportFrom}
      mailReportSubject: ${self:custom.config.mailReportSubject}
stepFunctions:
  stateMachines:
    reportMailer:
      Comment: Emails a daily report of the previous stats
      StartAt: GetHits
      States:
        GetHits:
          Type: Task
          Resource: reportGetHits
          Next: CompileReport
        CompileReport:
          Type: Task
          Resource: reportCompileReport
          Next: SendReport
        SendReport:
          Type: Task
          Resource: reportSendReport
          End: true

resources:
  Resources:
    HitregLogGroup:
      Properties:
        RetentionInDays: 30
    HitjsLogGroup:
      Properties:
        RetentionInDays: 30
    HitpngLogGroup:
      Properties:
        RetentionInDays: 30
    ReportTriggerLogGroup:
      Properties:
        RetentionInDays: 30
    ReportGetHitsLogGroup:
      Properties:
        RetentionInDays: 30
    ReportCompileReportLogGroup:
      Properties:
        RetentionInDays: 30
    ReportSendReportLogGroup:
      Properties:
        RetentionInDays: 30
    ApiGatewayMethodHitPost:
      Properties:
        RequestParameters:
          method.request.header.Origin: true
          method.request.header.User-Agent: false
          method.request.header.Dnt: false
    ApiGatewayMethodHitpngGet:
      Properties:
        RequestParameters:
          method.request.header.Referer: true
          method.request.header.User-Agent: false
          method.request.header.Dnt: false
    AnalyticsHitsDatabase:
      Type: AWS::SDB::Domain
      Properties:
        Description: "Hits database"
  Outputs:
     AnalyticsHitsDatabaseOutput:
       Description: "SimpleDB Domain"
       Value:
         "Ref": "AnalyticsHitsDatabase"
plugins:
  - serverless-step-functions
  - serverless-webpack
  - serverless-offline
  - serverless-mocha-plugin
  - serverless-aws-documentation

