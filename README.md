# Serverless Lambda Analytics

It's nice knowing if anyone is actually viewing your website. Google Analytics is a great product for that, but what if you don't feel like helping Google track your visitors, handing over information about them to Google? [Piwik](https://piwik.org/) is a great solution. But what if you're running a static website (perhaps with [serverless comments](https://gitlab.com/kabo/serverless-lambda-comments/)) and don't want to set up an entire server just for the purpose of analytics? This is where Serverless Lambda Analytics come in.

- [x] an endpoint where one can register views from javascript to SimpleDB
- [x] an endpoint that serves an image and registers in SimpleDB
- [ ] generate a javascript to include on pages to be tracked (with a noscript for the image)
- [x] a schedule that emails reports on an interval
- [ ] add description on how to download fakesdb.jar
- [ ] add description on how to setup config.[env].yml (or create a script that generates it?)
- [x] store cloudwatch logs 30 days
- [x] setup webpack compression
- [ ] clean up lambda to remove items older than X days

### Example

To run the example locally:

- `npm install`
- `npm run simpledb-local`
- `npm run test`
- `npm run offline` (run in new terminal)
- `npm run example-web-server` (run in new terminal)
- Go to `http://localhost:8081/example.html` in your browser

```
<script src="" async type="text/javascript"></script>
<noscript><img src="" /></noscript>
```

### License

Serverless lambda comments is released under the [BipCot License] v1.12 (https://bipcot.org/).

<a href="https://bipcot.org/"><img src="https://www.freedomfeens.com/wp-content/uploads/2016/04/bipicon.jpg" alt="bipicon" /></a>


