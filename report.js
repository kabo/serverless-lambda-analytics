/* eslint no-console: "off" */
'use strict'
const AWS = require('aws-sdk')
AWS.config.update({region: process.env.awsRegion})
const ses = new AWS.SES()
const moment = require('moment')
const nodemailer = require('nodemailer')
const sesTransport = require('nodemailer-ses-transport')
const store = require('./store')

const trigger = (event, context, callback) => {
  const stepfunctions = new AWS.StepFunctions()
  const yesterday = moment().utc().subtract(1, 'day')
  const params = {
    stateMachineArn: process.env.REPORTMAILER,
    input: JSON.stringify({
      from: yesterday.clone().startOf('day').toISOString(), 
      to: yesterday.clone().endOf('day').toISOString()
    })
  }
  stepfunctions.startExecution(params, function(err, data) {
    if (err) {
      console.log(err, err.stack) // an error occurred
      return callback(err)
    }
    //console.log(data)           // successful response
    callback(null, data)
  })
}
module.exports.trigger = trigger

const getHits = (event, context, callback) => {
  store.getHits({
    from: event.from, 
    to: event.to
  })
  .then((result) => {
    //console.log(result)
    callback(null, {hits: result}) // pass on to next step function
  })
  .catch(err => {
    console.error(err)
    callback(err)
  })
}

module.exports.getHits = getHits

const compileReport = (event, context, callback) => {
  const totNum = event.hits.length
  const mailText = `# hits yesterday: ${totNum}`
  callback(null, {mailText})
}

module.exports.compileReport = compileReport

const sendReport = (event, context, callback) => {
  const transport = nodemailer.createTransport(sesTransport({ ses }))
  const mailOptions = {
    from: process.env.mailReportFrom,
    to: process.env.mailReportTo,
    subject: process.env.mailReportSubject,
    text: event.mailText
  }
  transport.sendMail(mailOptions, (err, res) => {
    if (err) {
      console.error(err)
      return callback(err)
    }
    callback(null, res)
  })
}

module.exports.sendReport = sendReport

