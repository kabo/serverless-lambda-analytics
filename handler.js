/* eslint no-console: "off" */
'use strict'
const store = require('./store')

const originOk = (event) => {
  return event.headers.Origin &&
    new RegExp(process.env.allowedOriginsRegex).test(event.headers.Origin)
}

const checkOriginDecorator = (f) => {
  return (event, context, callback) => {
    if (event.headers.hasOwnProperty('origin') && !event.headers.hasOwnProperty('Origin')) {
      event.headers.Origin = event.headers.origin
    }
    console.log('event', event) 
    if (!originOk(event)) {
      const response = { statusCode: 403, body: JSON.stringify({
        err: `Origin '${event.headers.Origin}' denied`
      })}
      return callback(null, response)
    }
    return f(event, context, callback)
  }
}

const hit = (event, context, callback) => {
  if (typeof event.body === 'string') {
    event.body = JSON.parse(event.body)
  }
  const url = event.body.url
  // could add more data here collected by the javascript
  const dnt = event.headers.Dnt == '1'
  const values = {
    ip: dnt ? '' : event.requestContext.identity.sourceIp,
    useragent: dnt ? '' : event.headers['User-Agent'],
    url
  }
  store.saveHit(values)
  .then((result) => {
    console.log(result)
    const response = {
      statusCode: 204,
      headers: {
        'Access-Control-Allow-Origin' : event.headers.Origin
      },
      body: ''
    }
    callback(null, response)
  })
}

module.exports.hit = checkOriginDecorator(hit)

const hitpng = (event, context, callback) => {
  const url = event.headers.Referer
  const dnt = event.headers.Dnt == '1'
  const values = {
    ip: dnt ? '' : event.requestContext.identity.sourceIp,
    useragent: dnt ? '' : event.headers['User-Agent'],
    url
  }
  store.saveHit(values)
  .then((result) => {
    console.log(result)
    const response = {
      statusCode: 200,
      isBase64Encoded: true,
      headers: {
        'Content-Type': 'image/png'
      },
      body: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='
    }
    callback(null, response)
  })
}

module.exports.hitpng = hitpng

const hitjs = (event, context, callback) => {
  console.log(event)
  const response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/javascript'
    },
    body: [
      'if (!window.serverless_lambda_analytics_hit) {',
      '  window.serverless_lambda_analytics_hit = {};',
      '}',
      'serverless_lambda_analytics_hit = Object.assign(serverless_lambda_analytics_hit, {',
      '  url: window.location.href',
      '});',
      'fetch("//'+event.headers.Host+'/'+event.requestContext.stage+'/hit", {method: "POST", mode: "cors", headers: {"Content-Type":"application/json"}, body: JSON.stringify(serverless_lambda_analytics_hit)});'
    ].join('')
  }
  callback(null, response)
}

module.exports.hitjs = hitjs

