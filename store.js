'use strict'
const uuidV4 = require('uuid/v4')

module.exports = {
  saveHit: ({url, ip, useragent}) => {
    const AWS = require('aws-sdk')
    AWS.config.update({region: process.env.awsRegion})
    const simpledb = new AWS.SimpleDB({
      endpoint: process.env.simpleDbEndpoint,
      region: process.env.awsRegion
    })
    return new Promise((resolve, reject) => {
      const valid_url = new RegExp('^https?://[-a-zA-Z0-9:\.]{1,64}/').test(url)
      //console.log(url, valid_url);
      if (!valid_url) {
        return reject('url is invalid')
      }
      const id = uuidV4()
      const now_iso = new Date().toISOString()
      const params = {
        DomainName: process.env.simpleDbDomain,
        ItemName: id,
        Attributes: [
          {
            Name: 'CreatedAt',
            Value: now_iso
          },
          {
            Name: 'URL',
            Value: url
          },
          {
            Name: 'Useragent',
            Value: useragent
          },
          {
            Name: 'IP',
            Value: ip
          }
        ]
      }
      simpledb.putAttributes(params, (err, data) => {
        if (err) return reject(err)
        resolve({id})
      })
    })
  },
  getHits: ({from, to}) => {
    const AWS = require('aws-sdk')
    AWS.config.update({region: process.env.awsRegion})
    const simpledb = new AWS.SimpleDB({
      endpoint: process.env.simpleDbEndpoint,
      region: process.env.awsRegion
    })
    return new Promise((resolve, reject) => {
      const params = {
        SelectExpression: `select CreatedAt from \`${process.env.simpleDbDomain}\` `,
        ConsistentRead: false
      }
      if (from || to) {
        params.SelectExpression += 'where '
        if (from && to) {
          params.SelectExpression += `CreatedAt between '${from}' and '${to}'`
        } else if (from) {
          params.SelectExpression += `CreatedAt >= '${from}'`
        } else if (to) {
          params.SelectExpression += `CreatedAt <= '${to}'`
        }
      }
      simpledb.select(params, (err, data) => {
        if (err) return reject(err)
        if (!data.hasOwnProperty('Items')) return resolve([])
        const resp = data.Items.map(item => {
          return {
            id: item.Name, 
            CreatedAt: item.Attributes.find(a => a.Name === 'CreatedAt').Value,
            //IP: item.Attributes.find(a => a.Name === 'IP').Value
          }
        })
        resolve(resp)
      })
    })
  },
}
