'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

process.env.awsRegion = 'local'
process.env.simpleDbDomain = 'testdomain'
process.env.simpleDbEndpoint = 'http://localhost:8081'

const AWS = require('aws-sdk')
AWS.config.update({region: process.env.awsRegion})
const simpledb = new AWS.SimpleDB({
  endpoint: process.env.simpleDbEndpoint,
  region: process.env.awsRegion
})
//const moment = require('moment');
const _ = require('lodash')
const sleep = require('sleep')
const store = require('../store')
let t1, t2

describe('store', () => {
  before((done) => {
    new Promise((resolve, reject) => {
      const create_domain_params = {
        DomainName: '_flush'
      }
      simpledb.createDomain(create_domain_params, function(err, data) {
        if (err) return reject(err)
        return resolve(data)
      })
    })
    .then(() => {
      return new Promise((resolve, reject) => {
        const create_domain_params = {
          DomainName: process.env.simpleDbDomain
        }
        simpledb.createDomain(create_domain_params, function(err, data) {
          if (err) return reject(err)
          return resolve(data)
        })
      })
    })
    .then(() => {
      done()
    }).catch(err => {
      done(err)
    })
  })
  describe('saveHit', () => {
    it('can save a hit', (done) => {
      const values = {
        url: 'https://some.test.com/page.html',
        ip: '192.168.0.1',
        useragent: 'curl'
      }
      store.saveHit(values).then((data) => {
        expect(data).to.have.property('id')
        done()
      }).catch(err => {
        done(err)
      })
    })
    const values = {
      url: 'https://some.test.com/page2.html',
      ip: '192.168.0.1',
      useragent: 'curl'
    }
    it('validates that url is present', () => {
      return expect(store.saveHit(_.omit(values, ['url']))).to.be.rejected
    })
  })
  describe('getHits', () => {
    before(done => {
      const values = {
        url: 'https://some.test.com/page.html',
        ip: '192.168.0.10',
        useragent: 'curl'
      }
      store.saveHit(values).then(() => {
        t1 = new Date().toISOString()
        sleep.msleep(50)
        values.ip = '192.168.0.11'
        return store.saveHit(values)
      }).then(() => {
        t2 = new Date().toISOString()
        sleep.msleep(50)
        values.ip = '192.168.0.12'
        return store.saveHit(values)
      }).then(() => {
        done()
      }).catch(err => {
        done(err)
      })
    })
    it('can fetch all hits', done => {
      store.getHits({})
      .then(hits => {
        expect(hits).to.have.lengthOf(4)
        done()
      }).catch(err => {
        done(err)
      })
    })
    it('can fetch hits from date', done => {
      store.getHits({from: t1})
      .then(hits => {
        expect(hits).to.have.lengthOf(2)
        done()
      }).catch(err => {
        done(err)
      })
    })
    it('can fetch hits to date', done => {
      store.getHits({to: t2})
      .then(hits => {
        expect(hits).to.have.lengthOf(3)
        done()
      }).catch(err => {
        done(err)
      })
    })
    it('can fetch hits between dates', done => {
      store.getHits({from: t1, to: t2})
      .then(hits => {
        expect(hits).to.have.lengthOf(1)
        done()
      }).catch(err => {
        done(err)
      })
    })
    it('returns empty array when there are no hits', done => {
      store.getHits({from: '9', to: '0'})
      .then(hits => {
        expect(hits).to.have.lengthOf(0)
        done()
      }).catch(err => {
        done(err)
      })
    })
  })
})

