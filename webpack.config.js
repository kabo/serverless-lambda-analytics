'use strict'
var debug = process.env.NODE_ENV !== 'production'
var webpack = require('webpack')
var Visualizer = require('webpack-visualizer-plugin')
var path = require('path')

module.exports = {
  entry: {
    handler: './handler.js',
    report: './report.js'
  },
  target: 'node',
  externals: ['aws-sdk'],
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js', // this should match the first part of function handler in serverless.yml
  },
  plugins: [
    new Visualizer({ filename: '../webpack-stats.html' }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // to not to load all locales
    debug ? new webpack.optimize.UglifyJsPlugin({mangle: false, sourceMap: true, compress: false}) : new webpack.optimize.UglifyJsPlugin({mangle: true, sourceMap: false, compress: {warnings: false}})
  ]
}
